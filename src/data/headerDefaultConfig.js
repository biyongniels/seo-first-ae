import HeaderButton from "~sections/marketing/Header"
import React from "react";

const headerDefaultConfig = {
    headerClasses:
        "site-header site-header--menu-end light-header site-header--sticky",
    containerFluid: true,
    darkLogo: true,
    buttonBlock: (
        <HeaderButton
            className="ms-auto d-none d-xs-inline-flex"
            btnTwoText="Get Started"
            mr="15px"
            mrLG="0"
        />
    ),
}
export default  headerDefaultConfig
