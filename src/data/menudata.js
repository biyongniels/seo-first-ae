// import menuImage from '../assets/image/menu-image.webp'

export const menuItems = [
    {
        name: "digital",
        label: "Home",
    },
    {
        name: "",
        label: "Services",
        items: [
            { name: "project", label: "Branding/Logo" },
            { name: "agency", label: "Website Wireframes" },
            { name: "seo", label: "SEO" },
            { name: "it", label: "Web Development" },
            { name: "digital", label: "Digital Consulting Services" },
        ],
    },
    {
        name: "portfolio-masonary-3",
        label: "Portfolio",
    },
    {
        name: "contact-1",
        label: "Contact",
    },
];

export default menuItems;
