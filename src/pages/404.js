import React from "react";
import { Error } from "~sections/utility";
import { PageWrapper } from "~components/Core";
import FooterSection from "~sections/Common/Footer";


export default function errorPage() {
  return (
    <PageWrapper >
        <Error/>
        <FooterSection/>
    </PageWrapper>
  )
}

