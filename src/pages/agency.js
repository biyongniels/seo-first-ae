import React from "react";
import { PageWrapper } from "~components/Core";
import HeroSection from "~sections/agency/Hero";
import ServicesSection from "~sections/agency/Services";
import AboutSection from "~sections/agency/About";
import ContentSectionOne from "~sections/agency/ContentOne";
import PricingSection from "~sections/agency/Pricing";
import TestimonialSection from "~sections/agency/Testimonial";
import CtaSection from "~sections/agency/Cta";
import FooterFive from "~sections/Common/Footer";

export default function Agency() {
  return (
    <PageWrapper>
      <HeroSection />
      <ServicesSection />
      <AboutSection />
      <ContentSectionOne />
      <PricingSection />
      <TestimonialSection />
      <CtaSection />
      <FooterFive />
    </PageWrapper>
  );
}
