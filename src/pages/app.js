import React from "react";
import { PageWrapper } from "~components/Core";
import HeroSection from "~sections/app/Hero";
import ServicesSection from "~sections/app/Services";
import AboutSection from "~sections/app/About";
import ContentSectionOne from "~sections/app/ContentOne";
import PricingSection from "~sections/app/Pricing";
import TestimonialSection from "~sections/app/Testimonial";
import PromoSection from "~sections/app/Promo";
import CtaSection from "~sections/app/Cta";
import FooterFour from "~sections/Common/Footer";


export default function HomeApp() {
  return (
    <PageWrapper>
      <HeroSection />
      <ServicesSection />
      <AboutSection />
      <ContentSectionOne />
      <PricingSection />
      <TestimonialSection />
      <PromoSection />
      <CtaSection />
      <FooterFour />
    </PageWrapper>
  );
}
