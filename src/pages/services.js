import React from "react"
import { PageWrapper } from "~components/Core"
import HeroSection from '~sections/services/Hero'
import ServicesSection from '~sections/services/Services'
import AboutSection from '~sections/services/About'
import ContentSectionOne from '~sections/services/ContentOne'
import ProcessSection from '~sections/services/Process'
import PromoSection from '~sections/services/Promo'
import PricingSection from '~sections/services/Pricing'
import TestimonialSection from '~sections/services/Testimonial'
import ContactSection from '~sections/services/Contact'
import FooterFour from '~sections/Common/Footer'

export default function Services() {
  return (
    <PageWrapper>
      <HeroSection/>
      <ServicesSection/>
      <AboutSection/>
      <ContentSectionOne/>
      <ProcessSection/>
      <PromoSection/>
      <PricingSection/>
      <TestimonialSection/>
      <ContactSection/>
    <FooterFour/>
    </PageWrapper>
  )
}
