import React from "react";
import { PageWrapper } from "~components/Core";
import PortfolioSection from "~sections/portfolios/PortfolioMasonary/PortfolioMasonaryThree"
import FooterSection from "~sections/Common/Footer";

export default function portFolioMasonaryThreeNoGap() {
  return (
    <PageWrapper >
        <PortfolioSection gutters="false" background="#f3f4f6"/>
        <FooterSection/>
    </PageWrapper>
  )
}
