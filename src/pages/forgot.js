import React from "react";
import ResetSection from "~sections/Account/ResetPassword";
import { PageWrapper } from "~components/Core";

export default function ResetPassword() {
  return (
    <PageWrapper>
        <ResetSection/>
    </PageWrapper>
  )
}
