import React from "react";
import { PageWrapper } from "~components/Core";
import HeroSection from '~sections/project/Hero'
import FeatureSection from '~sections/project/Features'
import ContentOne from '~sections/project/ContentOne'
import ContentTwo from '~sections/project/ContentTwo'
import CtaSection from '~sections/project/Cta'
import FooterTwo from '~sections/Common/Footer'

export default function Project() {
  return (
    <PageWrapper>
        <HeroSection/>
        <FeatureSection/>
        <ContentOne/>
        <ContentTwo/>
        <CtaSection/>
        <FooterTwo />
    </PageWrapper>
  )
}
