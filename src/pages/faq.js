import React from "react";
import { PageWrapper } from "~components/Core";
import SearchSection from "~sections/utility/Faq/SearchSection";
import FaqSection from "~sections/utility/Faq/FaqSection";
import FooterSection from "~sections/Common/Footer";

export default function FaqPage() {
  return (
    <PageWrapper>
      <SearchSection/>
      <FaqSection/>
      <FooterSection/>
    </PageWrapper>
  )
}
