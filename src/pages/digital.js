import React from "react";
import { PageWrapper } from "~components/Core";
import HeroSection from "~sections/digital/Hero";
import ServicesSection from "~sections/digital/Services";
import AboutSection from "~sections/digital/About";
import ContentSectionOne from "~sections/digital/ContentOne";
import ContentSectionTwo from "~sections/digital/ContentTwo";
import TeamSection from "~sections/digital/Team";
import PortfolioSection from "~sections/digital/Portfolio";
import PromoSection from "~sections/digital/Promo";
import FooterSix from "~sections/Common/Footer";

export default function Digital() {
  return (
    <PageWrapper >
      <HeroSection />
      <ServicesSection />
      <AboutSection />
      <ContentSectionOne />
      <ContentSectionTwo />
      <TeamSection />
      <PortfolioSection />
      <PromoSection />
      <FooterSix />
    </PageWrapper>
  );
}
