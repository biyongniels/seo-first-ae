import React from "react";
import { PageWrapper } from "~components/Core";
import TermsConditionSection from "~sections/utility/TermsCondition"
import FooterSection from "~sections/Common/Footer";

export default function TermsAndConditions() {
  return (
    <PageWrapper >
        <TermsConditionSection/>
        <FooterSection/>
    </PageWrapper>
  )
}
