import React from "react";
import { PageWrapper } from "~components/Core"
import HeroSection from '~sections/it/Hero'
import ServicesSection from '~sections/it/Services'
import FeatureSection from '~sections/it/Feature'
import ContentSectionOne from '~sections/it/ContentOne'
import ProcessSection from '~sections/it/Process'
import CtaSection from '~sections/it/Cta'
import TestimonialSection from '~sections/it/Testimonial'
import ContactSection from '~sections/it/Contact'
import FooterThree from '~sections/Common/Footer'

export default function HomeIt() {
  return (
    <PageWrapper >
        <HeroSection/>
        <ServicesSection/>
        <FeatureSection/>
        <ContentSectionOne/>
        <ProcessSection/>
        <CtaSection/>
        <TestimonialSection/>
        <ContactSection/>
        <FooterThree/>
    </PageWrapper>
  )
}
