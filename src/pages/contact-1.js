import React from "react";
import ContactSection from "~sections/contact/ContactOne/ContactSection";
import FooterSection from "~sections/Common/Footer";
import { PageWrapper } from "~components/Core";

export default function contactOne() {
  return (
    <PageWrapper>
        <ContactSection/>
        <FooterSection/>
    </PageWrapper>
  )
}
