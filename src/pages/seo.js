import React from "react";
import { PageWrapper } from "~components/Core";
import HeroSection from '~sections/seo/Hero'
import FeatureSection from '~sections/seo/Features'
import ContentOne from '~sections/seo/ContentOne'
import ContentTwo from '~sections/seo/ContentTwo'
import CtaSection from '~sections/seo/Cta'
import FooterTwo from '~sections/Common/Footer'

export default function Seo() {
  return (
    <PageWrapper>
        <HeroSection/>
        <FeatureSection/>
        <ContentOne/>
        <ContentTwo/>
        <CtaSection/>
        <FooterTwo />
    </PageWrapper>
  )
}
