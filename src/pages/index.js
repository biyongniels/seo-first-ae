import React from "react"
import { PageWrapper } from "~components/Core"
import HeroSection from "~sections/marketing/Hero"
import ServiceSection from "~sections/marketing/Service"
import FeatureSection from "~sections/marketing/Features"
import ContentSectionOne from "~sections/marketing/ContentOne"
import ContentSectionTwo from "~sections/marketing/ContentTwo"
import TestimonialSection from "~sections/marketing/Testimonial"
import CounterSection from "~sections/marketing/Counter"
import CtaSection from "~sections/marketing/Cta"
import FooterOne from "~sections/Common/Footer"

export default function Marketing() {
  return (
      <PageWrapper>
        <HeroSection/>
        <ServiceSection/>
        <FeatureSection/>
        <ContentSectionOne/>
        <ContentSectionTwo/>
        <TestimonialSection/>
        <CounterSection/>
        <CtaSection/>
        <FooterOne/>
      </PageWrapper>
  )
}
