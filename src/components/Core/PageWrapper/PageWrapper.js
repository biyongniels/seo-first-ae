import React from "react";
import GlobalHeaderContext, { headerDefault } from "../../../context/GlobalHeaderContext";
import Link from "../Link";
import HeaderButton from "../Header/InnerPageHeader"
import Header from "../Header";
 const PageWrapper= ({ children, headerConfig = null, innerPage = false, innerPageFooter = false  })=> {
    const innerPageDefault = {
        headerClasses:
            "site-header site-header--menu-end light-header site-header--sticky",
        containerFluid: true,
        darkLogo: true,
        buttonBlock: (
            <HeaderButton
                className="ms-auto d-none d-xs-inline-flex"
                btnTwoText="Get Started"
                mr="15px"
                mrLG="0"
            />
        ),
    }
    const activeHeader = ( innerPage ? innerPageDefault : headerDefault );
  const sitectx = React.useContext(GlobalHeaderContext);

  React.useEffect(() => {
      sitectx.changeHeader({ ...activeHeader, ...headerConfig });
  },[headerConfig]);
  return (
    <>
     {/* <Header/> */}
      {children}
    </>
  )
}

export default PageWrapper;
