import { Link } from '~components'
import LogoBlack from "~image/logo/logo-black.png";
import React from 'react';
import Image from 'next/image';
import { Container, Row, Col } from 'react-bootstrap'
import Footer from "./style"
export default function FooterCommon(){
return(
    <Footer>
    <Container>
        <Footer.Box pbXL="95px">
        <Row>
            <Col xs="12" className="col-lg-4 col-md-8 col-xs-10">
                <Footer.Widgets className="footer-widgets footer-widgets--l7">
                {/* Brand Logo*/}
                <Footer.Box mb="30px">
                    <Link  to="/">
                        <img src={LogoBlack} alt="logo" />
                    </Link>
                </Footer.Box>
                <Footer.Text mb="36px">
                We’re the digital agency to create<br className="d-none d-xl-block" /> your digital presence for better<br className="d-none d-xl-block" /> conversion and sales.</Footer.Text>

            </Footer.Widgets>
            </Col>
            <Col xs="12" className="col-lg-8">
                <Row>
                    <Col xs="12" className="col-lg-6 col-xs-6">
                            <Footer.Widgets>
                                <Footer.Title>Contact Details</Footer.Title>
                                <Footer.Address className="widgets-address">
                                    <Footer.Address className="widgets-address">
                                        <Footer.AddressItem>
                                            <i className="fa fa-map-marker-alt" />
                                            <span>
                                              Dubaï
                                            </span>
                                        </Footer.AddressItem>
                                        <Footer.AddressItem>
                                            <i className="fa fa-phone-alt" />

                                            <a href="#">
                                                <span> Phone: </span>
                                                <br className="d-block" /> +99 (0) 101 0000 888
                                            </a>
                                        </Footer.AddressItem>
                                        <Footer.AddressItem>
                                            <i className="fa fa-envelope" />
                                            <a href="mailto:lily@seo-first.ae">
                                                <span className="d-block"> Email:</span>
                                                lily@seo-first.ae
                                            </a>
                                        </Footer.AddressItem>
                                    </Footer.Address>
                                </Footer.Address>
                            </Footer.Widgets>
                    </Col>
                    <Col xs="12" className="col-lg-6 col-xs-6">
                        <Footer.Widgets>
                            <Footer.Title>Company</Footer.Title>
                            <Footer.List>
                                <Footer.ListItems>
                                    <Link to="/about-us">About
                                    us</Link>
                                </Footer.ListItems>
                                <Footer.ListItems>
                                    <Link to="/">Privacy
                                    Policy</Link>
                                </Footer.ListItems>
                                <Footer.ListItems>
                                    <Link to="/terms">Terms &amp;
                                    Conditions</Link>
                                </Footer.ListItems>
                                <Footer.ListItems>
                                    <Link to="/faq">Faq</Link>
                                </Footer.ListItems>
                                <Footer.ListItems>
                                    <Link to="/contact-1">Contact</Link>
                                </Footer.ListItems>
                            </Footer.List>
                        </Footer.Widgets>
                    </Col>
                </Row>
            </Col>
        </Row>
        </Footer.Box>
        <Footer.Copyright>
        <Footer.CopyrightText>© 2022 SEO FIRST. All Rights Reserved</Footer.CopyrightText>
        </Footer.Copyright>
    </Container>
    </Footer>
)
}
